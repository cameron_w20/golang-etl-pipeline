package pipeline

import (
	"context"
	"gitlab.com/cameron_w20/golang-etl-pipeline/etl"
)

type Pipeline struct {
	Extractor   etl.Extractor
	Transformer etl.Transformer
	Loader      etl.Loader
}

func (p *Pipeline) Run(ctx context.Context) error {
	numOfSteps := 3
	input := make(chan etl.Input, 1)
	loaded := make(chan etl.Output, 1)
	errChan := make(chan error, numOfSteps)

	go func() {
		defer close(input)
		err := p.Extractor.Extract(ctx, input)
		errChan <- err
	}()

	go func() {
		defer close(loaded)
		err := p.Transformer.Transform(ctx, input, loaded)
		errChan <- err
	}()

	go func() {
		err := p.Loader.Load(ctx, loaded)
		errChan <- err
	}()

	for i := 0; i < numOfSteps; i++ {
		err := <-errChan
		if err != nil {
			return err
		}
	}

	return nil
}
