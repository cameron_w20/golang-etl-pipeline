package pipeline

import (
	"github.com/stretchr/testify/suite"
	"gitlab.com/cameron_w20/golang-etl-pipeline/mock/mock_etl"
	"testing"
)

type PipelineTestSuite struct {
	suite.Suite
	extractor   *mock_etl.Extractor
	transformer *mock_etl.Transformer
	loader      *mock_etl.Loader
	underTest   *Pipeline
}

func (ts *PipelineTestSuite) SetupTest() {
	ts.extractor = new(mock_etl.Extractor)
	ts.transformer = new(mock_etl.Transformer)
	ts.loader = new(mock_etl.Loader)
	ts.underTest = &Pipeline{
		Extractor:   ts.extractor,
		Transformer: ts.transformer,
		Loader:      ts.loader,
	}
}

func TestStatsTestSuite(t *testing.T) {
	suite.Run(t, new(PipelineTestSuite))
}
