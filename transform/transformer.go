package transform

import (
	"context"
	"github.com/golang/glog"
	"gitlab.com/cameron_w20/golang-etl-pipeline/etl"
)

var _ etl.Transformer = &TransformerImpl{}

type TransformerImpl struct{}

func NewTransformer() *TransformerImpl {
	return &TransformerImpl{}
}

func (t *TransformerImpl) Transform(_ context.Context, input <-chan etl.Input, output chan<- etl.Output) error {
	for data := range input {
		currencies := data.Data
		for _, currency := range currencies {
			entry := etl.Output{
				Name:              currency.Name,
				CmcRank:           currency.CmcRank,
				MaxSupply:         currency.MaxSupply,
				CirculatingSupply: currency.CirculatingSupply,
				Price:             currency.Quote.Gbp.Price,
				PercentChange1H:   currency.Quote.Gbp.PercentChange1H,
				LastUpdated:       currency.Quote.Gbp.LastUpdated,
			}
			glog.Infof("Loaded cryptocurrency %s", entry.Name)
			output <- entry
		}
	}
	return nil
}
