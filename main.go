package main

import (
	"context"
	"flag"
	"fmt"
	"gitlab.com/cameron_w20/golang-etl-pipeline/extract"
	"gitlab.com/cameron_w20/golang-etl-pipeline/load"
	"gitlab.com/cameron_w20/golang-etl-pipeline/pipeline"
	"gitlab.com/cameron_w20/golang-etl-pipeline/transform"
	"os"
)

const ApiKey = "Add API Key here"

func main() {
	flag.Parse()
	err := os.Setenv("CRYPTO_API_KEY", ApiKey)
	if err != nil {
		fmt.Printf("Failed to set env variable: %v", err)
	}

	ce := extract.NewCryptoExtractor("https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest")
	t := transform.NewTransformer()
	dbl := load.NewDBLoader()

	etlPipeline := pipeline.Pipeline{
		Extractor:   ce,
		Transformer: t,
		Loader:      dbl,
	}

	err = etlPipeline.Run(context.Background())
	if err != nil {
		fmt.Printf("Error in pipeline: %v", err)
	}
}
