package etl

//go:generate go run "github.com/vektra/mockery/cmd/mockery" -case=underscore -outpkg mock_etl -output ../mock/mock_etl -name=Extractor
//go:generate go run "github.com/vektra/mockery/cmd/mockery" -case=underscore -outpkg mock_etl -output ../mock/mock_etl -name=Transformer
//go:generate go run "github.com/vektra/mockery/cmd/mockery" -case=underscore -outpkg mock_etl -output ../mock/mock_etl -name=Loader

import (
	"context"
	"time"
)

type Input struct {
	Status struct {
		Timestamp    time.Time   `json:"timestamp"`
		ErrorCode    int         `json:"error_code"`
		ErrorMessage interface{} `json:"error_message"`
		Elapsed      int         `json:"elapsed"`
		CreditCount  int         `json:"credit_count"`
		Notice       interface{} `json:"notice"`
		TotalCount   int         `json:"total_count"`
	} `json:"status"`
	Data []struct {
		ID                            int         `json:"id"`
		Name                          string      `json:"name"`
		Symbol                        string      `json:"symbol"`
		Slug                          string      `json:"slug"`
		NumMarketPairs                int         `json:"num_market_pairs"`
		DateAdded                     time.Time   `json:"date_added"`
		Tags                          []string    `json:"tags"`
		MaxSupply                     int         `json:"max_supply"`
		CirculatingSupply             float64     `json:"circulating_supply"`
		TotalSupply                   float64     `json:"total_supply"`
		Platform                      interface{} `json:"platform"`
		CmcRank                       int         `json:"cmc_rank"`
		SelfReportedCirculatingSupply interface{} `json:"self_reported_circulating_supply"`
		SelfReportedMarketCap         interface{} `json:"self_reported_market_cap"`
		LastUpdated                   time.Time   `json:"last_updated"`
		Quote                         struct {
			Gbp struct {
				Price                 float64   `json:"price"`
				Volume24H             float64   `json:"volume_24h"`
				VolumeChange24H       float64   `json:"volume_change_24h"`
				PercentChange1H       float64   `json:"percent_change_1h"`
				PercentChange24H      float64   `json:"percent_change_24h"`
				PercentChange7D       float64   `json:"percent_change_7d"`
				PercentChange30D      float64   `json:"percent_change_30d"`
				PercentChange60D      float64   `json:"percent_change_60d"`
				PercentChange90D      float64   `json:"percent_change_90d"`
				MarketCap             float64   `json:"market_cap"`
				MarketCapDominance    float64   `json:"market_cap_dominance"`
				FullyDilutedMarketCap float64   `json:"fully_diluted_market_cap"`
				LastUpdated           time.Time `json:"last_updated"`
			} `json:"GBP"`
		} `json:"quote"`
	} `json:"data"`
}

type Output struct {
	Name              string
	CmcRank           int
	MaxSupply         int
	CirculatingSupply float64
	Price             float64
	PercentChange1H   float64
	LastUpdated       time.Time
}

type SegmentID string

type Extractor interface {
	Extract(context.Context, chan<- Input) error
}

type Transformer interface {
	Transform(context.Context, <-chan Input, chan<- Output) error
}

type Loader interface {
	Load(context.Context, <-chan Output) error
}
