package extract

import (
	"context"
	"encoding/json"
	"gitlab.com/cameron_w20/golang-etl-pipeline/etl"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
)

var _ etl.Extractor = &CryptoExtractor{}

type CryptoExtractor struct {
	Endpoint string
	ApiKey   string
}

func NewCryptoExtractor(url string) *CryptoExtractor {
	key := os.Getenv("CRYPTO_API_KEY")
	return &CryptoExtractor{
		Endpoint: url,
		ApiKey:   key,
	}
}

func (c CryptoExtractor) Extract(_ context.Context, inputs chan<- etl.Input) error {
	endpoint, err := c.makeRequestToCryptoEndpoint()
	if err != nil {
		return err
	}
	inputs <- endpoint
	return nil
}

func (c *CryptoExtractor) makeRequestToCryptoEndpoint() (etl.Input, error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", c.Endpoint, nil)
	if err != nil {
		return etl.Input{}, err
	}

	q := url.Values{}
	q.Add("start", "1")
	q.Add("limit", "5")
	q.Add("convert", "GBP")

	req.Header.Set("Accepts", "application/json")
	req.Header.Add("X-CMC_PRO_API_KEY", c.ApiKey)
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return etl.Input{}, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return etl.Input{}, err
	}

	data := etl.Input{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		return etl.Input{}, err
	}
	return data, nil
}
