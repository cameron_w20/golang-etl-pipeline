package load

import (
	"context"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/golang/glog"
	"github.com/stretchr/testify/suite"
	"gitlab.com/cameron_w20/golang-etl-pipeline/etl"
	"testing"
	"time"
)

type DBLoaderTestSuite struct {
	suite.Suite

	l *DBLoader

	output       chan etl.Output
	testCurrency etl.Output
}

func (ts *DBLoaderTestSuite) SetupTest() {
	ts.output = make(chan etl.Output, 1)
	ts.testCurrency = etl.Output{
		Name:              "test currency",
		CmcRank:           1,
		MaxSupply:         100,
		CirculatingSupply: 100,
		Price:             1,
		PercentChange1H:   0,
		LastUpdated:       time.Now(),
	}
}

func TestDBLoaderTestSuite(t *testing.T) {
	suite.Run(t, new(DBLoaderTestSuite))
}

func (ts *DBLoaderTestSuite) TestDBLoader_Load_OK() {
	//Given
	ts.output <- ts.testCurrency
	close(ts.output)

	db, mockdb, err := sqlmock.New()
	if err != nil {
		glog.Fatalf("error creating mock db: %v", err)
	}
	defer db.Close()
	mockdb.ExpectBegin()
	mockdb.ExpectExec("INSERT INTO cryptos").WillReturnResult(sqlmock.NewResult(1, 1))
	mockdb.ExpectCommit()

	//When
	err = ts.l.Load(context.TODO(), ts.output)

	//Then
	ts.NoError(err)
}
