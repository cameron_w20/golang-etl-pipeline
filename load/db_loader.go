package load

import (
	"context"
	"database/sql"
	"github.com/go-sql-driver/mysql"
	"github.com/golang/glog"
	"gitlab.com/cameron_w20/golang-etl-pipeline/etl"
)

var _ etl.Loader = &DBLoader{}

type DBLoader struct {
	User     string
	Password string
	Net      string
	Address  string
	DBName   string
}

func NewDBLoader() *DBLoader {
	return &DBLoader{
		User:     "dev",
		Password: "pass12345",
		Net:      "tcp",
		Address:  "localhost:3315",
		DBName:   "cryptoDB",
	}
}

func (l *DBLoader) Load(ctx context.Context, output <-chan etl.Output) error {
	db, err := l.connectToDB()
	if err != nil {
		return err
	}

	for data := range output {
		err = insertIntoDB(ctx, data, db)
		if err != nil {
			return err
		}
		glog.Infof("Added %s to db", data.Name)
	}
	db.Close()
	return nil
}

func (l *DBLoader) connectToDB() (*sql.DB, error) {
	cfg := mysql.Config{
		User:   l.User,
		Passwd: l.Password,
		Net:    l.Net,
		Addr:   l.Address,
		DBName: l.DBName,
	}

	db, err := sql.Open("mysql", cfg.FormatDSN())
	if err != nil {
		return nil, err
	}

	pingErr := db.Ping()
	if pingErr != nil {
		return nil, err
	}
	return db, nil
}

func insertIntoDB(ctx context.Context, cryptocurrency etl.Output, db *sql.DB) error {
	_, err := db.ExecContext(ctx, `INSERT INTO currencies (name, cmcRank, maxSupply, circulatingSupply, priceGBP, percentChangeOneH, lastUpdated) VALUES (?, ?, ?, ?, ?, ?, ?)`,
		cryptocurrency.Name, cryptocurrency.CmcRank, cryptocurrency.MaxSupply, cryptocurrency.CirculatingSupply,
		cryptocurrency.Price, cryptocurrency.PercentChange1H, cryptocurrency.LastUpdated)
	if err != nil {
		return err
	}
	return nil
}
