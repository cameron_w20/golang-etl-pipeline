CREATE DATABASE IF NOT EXISTS cryptoDB;

USE cryptoDB;

DROP TABLE IF EXISTS cryptoDB.currencies;

CREATE TABLE cryptoDB.currencies (id INT PRIMARY KEY AUTO_INCREMENT, name VARCHAR(255), cmcRank INT, maxSupply INT, circulatingSupply FLOAT, priceGBP FLOAT, percentChangeOneH FLOAT, lastUpdated TIMESTAMP);