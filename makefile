.PHONY: run-pipeline
.PHONY: unit-test
.PHONY: test-coverage

run-pipeline:
	scripts/run-pipeline.sh

test-unit:
	scripts/test-unit.sh

test-coverage:
	scripts/coverage-report.sh

## DB

build-db:
	docker-compose run --rm --name crypto_db -d -p 3315:3306 db

stop-db:
	docker stop crypto_db || exit 0

remove-db: stop-db
	docker rm crypto_db || exit 0

exec-db:
	docker exec -it crypto_db mysql -u dev -p"pass12345" cryptoDB

## Format
lint: vet fmt

vet:
	@go vet ./...
fmt:
	$(eval UNFMTD_FILES := $(shell gofmt -l .))
	@if [ "$(UNFMTD_FILES)" ]; then \
	  echo "ERROR: unformatted files:\n$(UNFMTD_FILES)"; \
	  exit 1; \
	fi

staticcheck:
	@go install honnef.co/go/tools/cmd/staticcheck@v0.2.2
	staticcheck ./...

dep:
	@go get -v -d ./...
